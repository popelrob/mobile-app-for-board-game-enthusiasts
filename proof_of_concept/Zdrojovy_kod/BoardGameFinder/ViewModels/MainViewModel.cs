﻿using Microsoft.Maui.Hosting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using BoardGameFinder.Views;
using Microsoft.Maui.Controls.Maps;
using Position = Microsoft.Maui.Devices.Sensors.Location;
using Microsoft.Maui.ApplicationModel;
using Microsoft.Maui.Maps;
using BoardGameFinder.CustomControls;
using System.Net.NetworkInformation;

namespace BoardGameFinder.ViewModels
{
    public class MainViewModel : BindableObject
    {
        public ObservableCollection<EventPin> Pins { get; } = new ObservableCollection<EventPin>();
        public ObservableCollection<EventViewModel> MyEvents { get; } = new ObservableCollection<EventViewModel>();
        public ObservableCollection<EventViewModel> AllEvents { get; } = new ObservableCollection<EventViewModel>();
        private string _username;
        private MainPage _mainPageReference;

        public MainViewModel()
        {
            ToggleCommand = new Command(ToggleButton);
            OnJoinClickedCommand = new Command(OnJoinClicked);
            OnCreateClickedCommand = new Command(OnCreateClicked);
            OnCancelClickedCommand = new Command(OnCancelClicked);
            OnMyEventsClickedCommand = new Command(OnMyEventsClicked);
            Application.Current.MainPage = new NavigationPage(new LoginPage(new LoginViewModel(this)));
            IsVisible = false;
            CreateVisible = false;
            IsButtonToggled = false;
            CreateTestEvents();
        }


        private bool _isButtonToggled;
        private EventPin _selectedPin;
        private string _place;
        private string _game;
        private string _owner;
        private int _participants;
        private int _maxParticipants;
        private bool _isVisible;
        private bool _joined;
        private bool _createVisible;
        private bool _myEventsVisible;
        private DateTime _date;

        public string Place { get { return _place; } set { _place = value; OnPropertyChanged(); } }
        public string Game { get { return _game; } set { _game = value; OnPropertyChanged(); } }
        public string Owner { get { return _owner; } set { _owner = value; OnPropertyChanged(); } }
        public int Participants { get { return _participants; } set { _participants = value; OnPropertyChanged(nameof(Participants)); } }
        public int MaxParticipants { get { return _maxParticipants; } set { _maxParticipants = value; OnPropertyChanged(nameof(MaxParticipants)); } }
        public bool IsVisible { get { return _isVisible; } set { _isVisible = value; OnPropertyChanged(nameof(IsVisible)); } }
        public bool Joined { get { return _joined; } set { _joined = value; OnPropertyChanged(nameof(Joined)); } }
        public bool CreateVisible { get { return _createVisible; } set { _createVisible = value; OnPropertyChanged(nameof(CreateVisible)); } }
        public bool MyEventsVisible { get { return _myEventsVisible; } set { _myEventsVisible = value; OnPropertyChanged(nameof(MyEventsVisible)); } }
        public EventPin SelectedPin { get => _selectedPin; set { _selectedPin = value; OnPropertyChanged(nameof(SelectedPin)); } }
        public DateTime Date { get => _date; set { _date = value; OnPropertyChanged(nameof(Date)); } }
        public bool IsButtonToggled
        {
            get => _isButtonToggled;
            set
            {
                if (_isButtonToggled != value)
                {
                    _isButtonToggled = value;
                    OnPropertyChanged(nameof(IsButtonToggled));
                }
            }
        }

        public ICommand ToggleCommand { get; }
        public void ToggleButton()
        {
            IsVisible = false;
            IsButtonToggled = !IsButtonToggled;
        }

        public ICommand OnJoinClickedCommand { get; }
        public void OnJoinClicked()
        {
            SelectedPin.Joined = !SelectedPin.Joined;
            SelectedPin.Participants = SelectedPin.Joined ? SelectedPin.Participants + 1 : SelectedPin.Participants - 1;
            Joined = SelectedPin.Joined;
            Participants = SelectedPin.Participants;

            foreach (var _event in AllEvents)
            {
                if (_event.LocationPin == SelectedPin)
                {
                    _event.Participants = SelectedPin.Participants;

                    if (SelectedPin.Joined)
                    {
                        _event.LocationPin = SelectedPin;
                        MyEvents.Add(_event);
                    }
                    else
                    {
                        MyEvents.Remove(_event);
                    }
                }
            }
        }

        public ICommand OnCreateClickedCommand { get; }
        public void OnCreateClicked()
        {
            var pin = SelectedPin;
            pin.Label = Place;
            pin.Game = Game;
            pin.MaxParticipants = MaxParticipants;

            var _event = new EventViewModel(this)
            {
                Place = pin.Label,
                Game = pin.Game,
                Participants = pin.Participants,
                MaxParticipants = pin.MaxParticipants,
                LocationPin = pin,
                Owner = pin.Owner,
                Date = pin.Date
            };

            AllEvents.Add(_event);
            MyEvents.Add(_event);
            CreateVisible = false;
            IsVisible = false;
        }

        public ICommand OnCancelClickedCommand { get; }
        public void OnCancelClicked()
        {
            Pins.Remove(SelectedPin);
            CreateVisible = false;
        }

        public ICommand OnMyEventsClickedCommand { get; }
        public void OnMyEventsClicked()
        {
            MyEventsVisible = !MyEventsVisible;
            CreateVisible = false;
            IsVisible = false;
        }

        public void HandleMapTapped(Position position)
        {
            if (IsButtonToggled == false) {
                IsVisible = false;
                return;
            }

            SelectedPin = CreatePin(position);
            SelectedPinChanged();
            CreateVisible = true;
            IsButtonToggled = false;
        }

        private EventPin CreatePin(Position position)
        {
            var pin = new EventPin()
            {
                Label = "",
                Location = position,
                Type = PinType.Generic,
                Address = $"Lat: {position.Latitude}, Lng: {position.Longitude}",
                Game = "",
                Participants = 1,
                MaxParticipants = 2,
                Joined = true,
                Owner = _username,
                Date = DateTime.Now
            };
            Pins.Add(pin);
            return pin;
        }

        public void HandlePinTapped(object sender, PinClickedEventArgs e)
        {
            EventPin pin = (EventPin)sender;
            SelectedPin = pin;
            SelectedPinChanged();
            IsVisible = true;
        }

        private void SelectedPinChanged()
        {
            Place = SelectedPin.Label;
            Game = SelectedPin.Game;
            Participants = SelectedPin.Participants;
            MaxParticipants = SelectedPin.MaxParticipants;
            Joined = SelectedPin.Joined;
            Owner = SelectedPin.Owner;
            Date = SelectedPin.Date;
        }

        public bool SetUser(string username)
        {
            _username = username;
            return true;
        }

        private void CreateTestEvents()
        {
            var position = new Position(55.676098, 12.568337);
            var position1 = new Position(50.676098, 12.568337);
            var pin = new EventPin()
            {
                Label = "test1",
                Location = position,
                Type = PinType.Generic,
                Address = $"Lat: {position.Latitude}, Lng: {position.Longitude}",
                Game = "test1.1",
                Participants = 1,
                MaxParticipants = 3,
                Joined = false,
                Owner = "tester1",
                Date = DateTime.Now
            };
            var pin1 = new EventPin()
            {
                Label = "test2",
                Location = position1,
                Type = PinType.Generic,
                Address = $"Lat: {position1.Latitude}, Lng: {position1.Longitude}",
                Game = "test2.1",
                Participants = 1,
                MaxParticipants = 2,
                Joined = false,
                Owner = "tester2",
                Date = DateTime.Now
            };

            Pins.Add(pin1);
            Pins.Add(pin);

            var _event = new EventViewModel(this)
            {
                Place = pin.Label,
                Game = pin.Game,
                Participants = pin.Participants,
                MaxParticipants = pin.MaxParticipants,
                LocationPin = pin,
                Owner = pin.Owner,
                Date = pin.Date
            };

            var _event1 = new EventViewModel(this)
            {
                Place = pin1.Label,
                Game = pin1.Game,
                Participants = pin1.Participants,
                MaxParticipants = pin1.MaxParticipants,
                LocationPin = pin1,
                Owner = pin1.Owner,
                Date = pin1.Date
            };

            AllEvents.Add(_event);
            AllEvents.Add(_event1);
        }

        public void MoveMap(Location location)
        {
            MyEventsVisible = false;
            _mainPageReference.MoveMap(location);
        }

        public void SetMainPageReference(MainPage mainPage)
        {
            _mainPageReference = mainPage;
        }
    }
}
