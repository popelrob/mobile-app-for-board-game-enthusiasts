﻿using BoardGameFinder.CustomControls;
using Microsoft.Maui.Controls.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BoardGameFinder.ViewModels
{
    public class EventViewModel : BindableObject
    {
        private MainViewModel _mainViewModelReference;
        public ICommand RemoveEvent { get; private set; }
        private void RemoveEventHandler(object obj)
        {
            _locationPin.Joined = false;
            _locationPin.Participants--;
            _mainViewModelReference.MyEvents.Remove(this);
        }

        public ICommand ShowPinEvent { get; private set; }
        private void ShowPinEventHandler(object obj)
        {
            _mainViewModelReference.MoveMap(_locationPin.Location);
        }

        public EventViewModel(MainViewModel mainViewModelReference)
        {
            RemoveEvent = new Command(RemoveEventHandler);
            ShowPinEvent = new Command(ShowPinEventHandler);

            _mainViewModelReference = mainViewModelReference;
        }

        private EventPin _locationPin;
        private string _place;
        private string _game;
        private string _owner;
        private int _participants;
        private int _maxParticipants;
        private DateTime _date;

        public EventPin LocationPin { get => _locationPin; set { _locationPin = value; OnPropertyChanged(nameof(LocationPin)); } }
        public string Place { get { return _place; } set { _place = value; OnPropertyChanged(); } }
        public string Game { get { return _game; } set { _game = value; OnPropertyChanged(); } }
        public string Owner { get { return _owner; } set { _owner = value; OnPropertyChanged(); } }
        public int Participants { get { return _participants; } set { _participants = value; OnPropertyChanged(); } }
        public int MaxParticipants { get { return _maxParticipants; } set { _maxParticipants = value; OnPropertyChanged(); } }
        public DateTime Date { get { return _date; } set { _date = value; OnPropertyChanged(); } }

     
    }
}
