﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BoardGameFinder.ViewModels
{
    public class LoginViewModel : BindableObject
    {
        private string _username;
        private string _password;
        private MainViewModel _mainViewModelReference;

        public string Username
        {
            get => _username;
            set
            {
                if (_username != value)
                {
                    _username = value;
                    OnPropertyChanged(nameof(Username));
                }
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (_password != value)
                {
                    _password = value;
                    OnPropertyChanged(nameof(Password));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICommand LoginCommand { get; }

        public LoginViewModel(MainViewModel mainView)
        {
            LoginCommand = new Command(OnLogin);
            _mainViewModelReference = mainView;
        }

        public LoginViewModel()
        {
            LoginCommand = new Command(OnLogin);
        }

        private async void OnLogin()
        {
            // Validate credentials
            bool isValidCredentials = true;

            if (isValidCredentials)
            {
                _mainViewModelReference.SetUser(Username);
                Application.Current.MainPage = new MainPage(_mainViewModelReference);
            }
            else
            {
                // Show an error message or handle authentication failure
                // For example: DisplayAlert("Error", "Invalid credentials", "OK");
            }
        }
    }
}
