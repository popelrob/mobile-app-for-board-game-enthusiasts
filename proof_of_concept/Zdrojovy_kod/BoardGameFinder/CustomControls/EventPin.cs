﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoardGameFinder.Views;
using Microsoft.Maui.Controls.Maps;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BoardGameFinder.CustomControls
{
    public class EventPin : Pin
    {
        public EventPin()
        {

        }

        public static readonly BindableProperty GameProperty = BindableProperty.Create(nameof(Game), typeof(string), typeof(EventPin), string.Empty);
        public string Game
        {
            get => (string)GetValue(EventPin.GameProperty);
            set => SetValue(EventPin.GameProperty, value);
        }

        public static readonly BindableProperty ParticipantsProperty = BindableProperty.Create(nameof(Participants), typeof(int), typeof(EventPin), 0);
        public int Participants
        {
            get => (int)GetValue(EventPin.ParticipantsProperty);
            set => SetValue(EventPin.ParticipantsProperty, value);
        }

        public static readonly BindableProperty MaxParticipantsProperty = BindableProperty.Create(nameof(MaxParticipants), typeof(int), typeof(EventPin), 0);
        public int MaxParticipants
        {
            get => (int)GetValue(EventPin.MaxParticipantsProperty);
            set => SetValue(EventPin.MaxParticipantsProperty, value);
        }

        public static readonly BindableProperty OwnerProperty = BindableProperty.Create(nameof(Owner), typeof(string), typeof(EventPin), string.Empty);
        public string Owner
        {
            get => (string)GetValue(EventPin.OwnerProperty);
            set => SetValue(EventPin.OwnerProperty, value);
        }

        public static readonly BindableProperty JoinedProperty = BindableProperty.Create(nameof(Joined), typeof(bool), typeof(EventPin), false);
        public bool Joined
        {
            get => (bool)GetValue(EventPin.JoinedProperty);
            set => SetValue(EventPin.JoinedProperty, value);
        }

        public static readonly BindableProperty DateProperty = BindableProperty.Create(nameof(Date), typeof(DateTime), typeof(EventPin), DateTime.MinValue);
        public DateTime Date
        {
            get => (DateTime)GetValue(EventPin.DateProperty);
            set => SetValue(EventPin.DateProperty, value);
        }
    }

}
