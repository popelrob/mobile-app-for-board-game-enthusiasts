using BoardGameFinder.ViewModels;

namespace BoardGameFinder.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage(LoginViewModel loginVM)
        {
            InitializeComponent();
            BindingContext = loginVM;
        }
    }
}
