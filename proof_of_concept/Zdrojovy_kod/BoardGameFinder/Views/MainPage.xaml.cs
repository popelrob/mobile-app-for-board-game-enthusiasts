﻿using BoardGameFinder.Views;
using BoardGameFinder.ViewModels;
using Microsoft.Maui.Controls.Maps;
using Microsoft.Maui.Maps;

namespace BoardGameFinder
{
    public partial class MainPage : ContentPage
    {
        MainViewModel vm;
        public MainPage()
        {
            InitializeComponent();
            GetCurrentLocation();
            vm = new MainViewModel();
            vm.SetMainPageReference(this);
            BindingContext = vm;
        }

        public MainPage(MainViewModel viewModel)
        {
            InitializeComponent();
            GetCurrentLocation();
            vm = viewModel;
            vm.SetMainPageReference(this);
            BindingContext = vm;
        }

        private async void OnDropdownClicked(object sender, EventArgs e)
        {
            if (DropdownMenu.IsVisible)
            {
                await DropdownMenu.TranslateTo(0, -DropdownMenu.Height, 250, Easing.SinInOut);
                DropdownMenu.IsVisible = false;
            }
            else
            {
                DropdownMenu.IsVisible = true;
                await DropdownMenu.TranslateTo(0, 0, 250, Easing.SinInOut);
            }
        }

        private void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            vm.HandleMapTapped(e.Location);
        }

        private void Pin_MarkerClicked(object sender, PinClickedEventArgs e)
        {
            // Show the dropdown menu when a pin is clicked
            vm.HandlePinTapped(sender, e);
        }

        private CancellationTokenSource _cancelTokenSource;

        private async Task GetCurrentLocation()
        {
            try
            {

                GeolocationRequest request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(10));

                _cancelTokenSource = new CancellationTokenSource();

                Location location = await Geolocation.Default.GetLocationAsync(request, _cancelTokenSource.Token);

                if (location != null)
                {
                    MapSpan mapSpan = new MapSpan(location, 0.01, 0.01);
                    map.MoveToRegion(mapSpan);
                }
            }
            // Catch one of the following exceptions:
            //   FeatureNotSupportedException
            //   FeatureNotEnabledException
            //   PermissionException
            catch (Exception ex)
            {
                // Unable to get location
            }
        }

        internal void MoveMap(Location location)
        {
            MapSpan mapSpan = MapSpan.FromCenterAndRadius(location, Distance.FromKilometers(0.1));
            map.MoveToRegion(mapSpan);
        }
    }

}
