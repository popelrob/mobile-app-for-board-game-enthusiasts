namespace BoardGameFinder.Views
{
    public partial class EventMenuCV : ContentView
    {
        public EventMenuCV()
        {
            InitializeComponent();
        }

        private bool _viewMode;


        public bool ViewMode { get { return _viewMode; } set { _viewMode = value; OnPropertyChanged(); } }

        public static readonly BindableProperty PlaceProperty = BindableProperty.Create(nameof(Place), typeof(string), typeof(EventMenuCV), string.Empty);
        public string Place
        {
            get => (string)GetValue(EventMenuCV.PlaceProperty);
            set => SetValue(EventMenuCV.PlaceProperty, value);
        }

        public static readonly BindableProperty GameProperty = BindableProperty.Create(nameof(Game), typeof(string), typeof(EventMenuCV), string.Empty);
        public string Game
        {
            get => (string)GetValue(EventMenuCV.GameProperty);
            set => SetValue(EventMenuCV.GameProperty, value);
        }

        public static readonly BindableProperty ParticipantsProperty = BindableProperty.Create(nameof(Participants), typeof(int), typeof(EventMenuCV), 0);
        public int Participants
        {
            get => (int)GetValue(EventMenuCV.ParticipantsProperty);
            set => SetValue(EventMenuCV.ParticipantsProperty, value);
        }

        public static readonly BindableProperty OwnerProperty = BindableProperty.Create(nameof(Owner), typeof(string), typeof(EventMenuCV), string.Empty);
        public string Owner
        {
            get => (string)GetValue(EventMenuCV.OwnerProperty);
            set => SetValue(EventMenuCV.OwnerProperty, value);
        }

        public static readonly BindableProperty MaxParticipantsProperty = BindableProperty.Create(nameof(MaxParticipants), typeof(int), typeof(EventMenuCV), 0);
        public int MaxParticipants
        {
            get => (int)GetValue(EventMenuCV.MaxParticipantsProperty);
            set => SetValue(EventMenuCV.MaxParticipantsProperty, value);
        }

        public static readonly BindableProperty DateProperty = BindableProperty.Create(nameof(Date), typeof(DateTime), typeof(EventMenuCV), DateTime.Now);
        public DateTime Date
        {
            get => (DateTime)GetValue(EventMenuCV.DateProperty);
            set => SetValue(EventMenuCV.DateProperty, value);
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            ViewMode = !ViewMode;
        }

        public event EventHandler? Remove;

        private void Button_Clicked(object sender, EventArgs e)
        {
            Remove?.Invoke(this, e);
        }

        public event EventHandler? ShowPin;

        private void ShowPinClicked(object sender, EventArgs e)
        {
            ShowPin?.Invoke(this, e);
        }
    }
}
